import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
//import { Link, Route, Switch } from 'react-router-dom';
import Graph1 from '../graph1';
import Graph2 from '../graph2';
import Graph3 from '../graph3';
import Graph4 from '../graph4';
import Graph5 from '../graph5';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  getGraph(id, data){
    if(id==1) return  <Graph1 {...data.graph1} />
    if(id==2) return  <Graph2 {...data.graph2} />
    if(id==3) return  <Graph3 {...data.graph3} />
    if(id==4) return  <Graph4 {...data.graph4} />
    if(id==5) return  <Graph5 {...data.graph5} />
    return  <Graph1 {...data.graph1} />
  }

  render() {
    const { data } = this.props;
    let idtype = this.props.idtype;
    let renderGraph  = this.getGraph(idtype, data)

    return (
        <MuiThemeProvider>
          <div className="pm-container">
            {renderGraph}
          </div>
        </MuiThemeProvider>
    );
  }


/*
  render() {
    const { data } = this.props;
    
    return (
        <MuiThemeProvider>
          <div className="container">
           <Route path="/main" render={() => ( 
              <nav>
                <ul>
                  <li><Link to='/1'>infographic 1</Link></li>
                  <li><Link to='/2'>infographic 2</Link></li>
                  <li><Link to='/3'>infographic 3</Link></li>
                  <li><Link to='/4'>infographic 4</Link></li>
                  <li><Link to='/5'>infographic 5</Link></li>
                </ul>
              </nav>
            )}/>
           <Route path="/1" render={() => ( <Graph1 {...data.graph1} />)}/>
           <Route path="/2" render={() => ( <Graph2 {...data.graph2} />)}/>
           <Route path="/3" render={() => ( <Graph3 {...data.graph3} />)}/>
           <Route path="/4" render={() => ( <Graph4 {...data.graph4} />)}/>
           <Route path="/5" render={() => ( <Graph5 {...data.graph5} />)}/>
          </div>
        </MuiThemeProvider>
    );
  }
*/



}
