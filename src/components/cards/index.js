import React from "react"
import * as d3 from 'd3';
import bind from '../../utils/bind';
// import _ from 'underscore'

export default class Cards extends React.Component {

  constructor(props) {
    super(props);
  
    this.state = {
      country: props.country,
      wd: document.getElementById("powerman2-app-root").offsetWidth
    };
    bind(this, 'handleResize', 'createChart', 'createData');
    this.colors = ["#466ea5", "#6390c6", "#6e8f82"];

    this.names = [
        "Skilled Trades",
        "Sales Reps",
        "Engineers",
        "Drivers",
        "Technicians",
        "IT Staff",
        "Accounting and Finance Staff",
        "White Collar Professionals",
        "Office Support Staff",
        "Production Operators / Machine Operators",
        "Management / Executive (Management / Corporate)",
        "Health Care Professionals",
        "Restaurants & Hotel staff",
        "Teachers",
        "Customer Support",
        "Cleaners & Domestic staff",
        "Security Guards",
        "Supervisors",
        "Laborers"
      ]

    this.names2 = [
        "Accounting and Finance Staff",
        "Cleaners & Domestic staff",
        "Customer Support",
        "Drivers",
        "Engineers",
        "Health Care Professionals",
        "IT Staff",
        "Laborers",
        "Management / Executive (Management / Corporate)",
        "Production Operators / Machine Operators",
        "Office Support Staff",
        "White Collar Professionals",
        "Restaurants & Hotel staff",
        "Sales Reps",
        "Security Guards",
        "Skilled Trades",
        "Supervisors",
        "Teachers",
        "Technicians"
      ]

      // let inter = _.intersection(this.names, this.names2)

      // let diff = _.difference(this.names, inter)
      // let diff2 = _.difference(this.names2, inter)


      // console.log(diff, diff2)


    this.preffix = 'https://cdn2.hubspot.net/hubfs/4649810/infographic-assets/'//'assets/img/'
    this.preffix = 'assets/img/'
  }

  componentDidMount() {     
    window.addEventListener('resize', this.handleResize);
    this.createChart();
  }

  handleResize() {
    if(document.getElementById("powerman2-app-root").offsetWidth != this.state.wd){
      this.setState({
        wd: document.getElementById("powerman2-app-root").offsetWidth
      })

      d3.select(".pm-graph2-div").selectAll("*").remove();
      this.createChart();
    }
  }

  createChart(){
      this.g = d3.select(".pm-graph2-div");
      this.redrawGraph()   
  }

  componentDidUpdate() {
      this.redrawGraph()
  }

  redrawGraph(){
      let dt = this.createData(this.props.data[this.props.country]);
      this.drawGraph(dt)
  }

  drawGraph(dt){

    const that = this
    const wd = (this.state.wd < 1350) ? this.state.wd : 1350
    const mrg = (this.state.wd <= 800) ? 10 : 25
    const size = (this.state.wd <= 800) ? (wd - 1*mrg)/2 : (wd - 4*mrg)/5;
    const n = (this.state.wd <= 800) ? 2 : 5 // elements per row

    // cards
    let cards = this.g.selectAll(".pm-cards").data(dt, function(d) { return d.name; })

    cards.enter()
        .append("div")
        .attr("class", "pm-cards")

        .style('visibility', (d, i)=>{ return ((wd <= 800) && (i==9)) ? 'visible' : 'visible' })
        .style('background-image', (d, i)=>{
          return 'url('+that.preffix+'roles'+that.names.indexOf(d.name)+ ((that.state.wd <= 800)?"m":"") + '.svg)'})  
        .html((d, i)=>{ return '<div class="pm-g2circle"><div class="pm-g2span">' +  (i+1) + '</div></circle>' })
        .style("width", (size - 30) + 'px')
        .style("height", (size - 30) + 'px')
        .style("opacity", 0)
        .style("top", (d, i)=>{
          const position = (i - i%n)/n
          const top = ( position * (size + mrg) + 15)  + 'px'
          return  top
        })
        .style("left", (d, i)=>{
          const position = i%n
          const left = ( position * (size + mrg) + 15) + 'px'
          return left
        })
        .style('background-color', (d, i)=>{return that.colors[i%((wd<=800)?2:3)]})
        .transition()
        .ease(d3.easeCubic)
        .duration(700)
        .style("width", size + 'px')
        .style("height", size + 'px')
        .style("top", (d, i)=>{
          const position = (i - i%n)/n
          const top = ( position * (size + mrg)) + 'px'
          return  top
        })
        .style("left", (d, i)=>{
          const position = i%n
          const left = ( position * (size + mrg)) + 'px'
          return left
        })  
        .style("opacity", 1)   


    cards
        .style('visibility', (d, i)=>{
          return ((wd <= 800) && (i==9)) ? 'visible' : 'visible' 
        }) 
        .html((d, i)=>{ return '<div class="pm-g2circle"><div class="pm-g2span">' +  (i+1) + '</div></circle>' })
        .transition()
        .ease(d3.easeCubic)
        .duration(700)
        .style('background-color', (d, i)=>{return that.colors[i%((wd<=800)?2:3)]}) 
        .style("top", (d, i)=>{
          const position = (i - i%n)/n
          const top = ( position * (size + mrg)) + 'px'
          return  top
        })
        .style("left", (d, i)=>{
          const position = i%n
          const left = ( position * (size + mrg)) + 'px'
          return left
        })   

    cards.exit()
        .remove()

  }

  createData(dt) {
    let data = []
    for(let i in dt) { 
        data.push({
            name: this.names2[i],
            val: dt[i]
          });
    }
    data.sort((d1, d2)=>{ return d2.val - d1.val; })
    data = data.slice(0, 10)
    


    return data
  }

  
  render() {
    const wd = (this.state.wd < 1350) ? this.state.wd : 1350
    const mrg = (this.state.wd <= 800) ? 10 : 25
    const size = (this.state.wd <= 800) ? (wd - 1*mrg)/2 : (wd - 4*mrg)/5;
    const svgWd = (this.state.wd <= 800) ? 3*size + 2*mrg : 5*size + 4*mrg;
    const svgHg = (this.state.wd <= 800) ? 5*size + 4*mrg : 2*size + 1*mrg;
    
    const divStyle = {
      width: svgWd + 'px',
      height: svgHg + 'px'
    }


    return (
      <div className="pm-graph2-div" style={divStyle}></div>
    );
  }

}
